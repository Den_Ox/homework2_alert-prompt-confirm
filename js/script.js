'use strict';

//
/* Які існують типи даних у Javascript?
1. boolean, string, number, null, undefined, function, object, bigInt.

2. У чому різниця між == і ===?
== - это нестрогое сравнение, === - строгое сравнение. 
=== не приводит значения к одному типу, поэтому для проверок рекомендовано использовать строгое сравнение. 

3. Що таке оператор?
Я понимаю оператор как элемент описания действия (сложения, вычитания, деления, сравнения и т.д.) операндов (аргументов).
Есть много типов операторов, например операторы присваивания, сравнения, арифметические, логические и т.д.

*/

/* Option 1 */

// let firstName;

// let age;

// do {
//   firstName = prompt('write your NAME, pls', firstName);
// } while (firstName == Number(firstName));

// do {
//   age = +prompt('write your AGE, pls', age;)
// } while (!age);

// if (age >= 18 && age <= 22) {
//   confirm('Are you want to continue?');
//   alert(`Welcome ${firstName}`);
// } else if (age > 22) {
//   alert(`Welcome ${firstName}`);
// } else {
//   alert('You are not allawed to visit a website');
// }

/* Option 2 */

let firstName = prompt('write your name');
while (firstName == Number(firstName)) {
  firstName = prompt('write PLEASE your NAME', firstName);
}

let age = +prompt('write your age');
while (!age) {
  age = +prompt('write PLEASE your AGE', age); // даёт NaN, а не вписанные символы, подскажите, пожалуйста, как исправить.
}

if (age >= 18 && age <= 22) {
  confirm('Are you want to continue?');
  alert(`Welcome ${firstName}`);
} else if (age > 22) {
  alert(`Welcome ${firstName}`);
} else {
  alert('You are not allawed to visit a website');
}

console.log(firstName);
console.log(typeof firstName);
console.log(age);
console.log(typeof age);
